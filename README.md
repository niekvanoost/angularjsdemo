# README #

Simple CRUD demo application in AngularJS.

### Created with ###

* [Codeigniter](https://www.codeigniter.com)
* [Restfull server for Codeigniter](https://github.com/chriskacerguis/codeigniter-restserver)
* [Bootstrap](http://getbootstrap.com)

### How to setup ###

* Create a mySQL database
* import the file /SQL/users.sgl into the created databes
* Change the database connection settings in /api/application/config/database.php
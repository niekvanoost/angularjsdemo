'use strict';

var blueDonutApp = angular.module('blueDonutApp', [
    'ngRoute',
    'ui.bootstrap'
]);



blueDonutApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider){
        $routeProvider.
            when("/home", {
                templateUrl: "pages/home.html",
                controller: "homeController"
            }).
            when("/user", {
                templateUrl: "pages/users.html",
                controller: "userController"
            }).
            when("/user/edit/:userId", {
                templateUrl: "pages/editUser.html",
                controller: "userEditController"
            }).
            when("/user/add",{
                templateUrl: "pages/addUser.html",
                controller: "userAddController"
            }).
            otherwise({
                redirectTo: "/home"
            });
           // $locationProvider.html5Mode(true);
    }]);
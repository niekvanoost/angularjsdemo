blueDonutApp.controller('userController', ['$scope', '$routeParams', '$http', '$location', '$rootScope',
    function($scope, $routeParams, $http, $location, $rootScope){
       
        $scope.loadUsers = function(){
            $http({
                url: 'api/index.php/user',
                method: 'GET'
            }).
            success(function(data){
                $scope.users = data;
            }).error(function(data){
                $scope.users = data;
            });
       };

       $scope.loadUsers();

       $scope.editUser = function(id){
           $location.path('/user/edit/' + id);
       };

       $scope.addUser = function(){
           $location.path('/user/add');
       }

        $scope.deleteUser = function(id){
            if (confirm("Are you sure to delete this user?")){
                $http({
                    url: 'api/index.php/user/deleteUser',
                    method: 'POST',
                    data: {id: id}
                }).then(function successCallback(response){
                    $scope.loadUsers();
                });
            }
       }
    }
]);
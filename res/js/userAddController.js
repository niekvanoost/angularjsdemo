blueDonutApp.controller('userAddController', ['$scope', '$routeParams', '$http', '$location', '$rootScope',
    function($scope, $routeParams, $http, $location, $rootScope){
        
        $scope.submit = function(){
            var data = {
                id: $scope.userId,
                realname: $scope.user.realname,
                username: $scope.user.username
            };

            $http({
                url: 'api/index.php/user',
                method: 'PUT',
                data: data
            }).then(function successCallback(response){
                $location.path('/user');
            })
        };

        $scope.cancel = function(){
            $location.path('/user');
        }


    }
]);
blueDonutApp.controller('userEditController', ['$scope', '$routeParams', '$http', '$location', '$rootScope',
    function($scope, $routeParams, $http, $location, $rootScope){
        $scope.userId = $routeParams.userId;

        $scope.loadUser = function(){
            $http({
            url: 'api/index.php/user',
            method: 'GET',
            params: {id: $scope.userId}
            }).then(function successCallback(response){
                $scope.user = response.data;
            });
        };

        $scope.loadUser();
        

        $scope.submit = function(){

            var data = {
                id: $scope.userId,
                realname: $scope.user.realname,
                username: $scope.user.username
            };

            $http({
                url: 'api/index.php/user',
                method: 'POST',
                data: data
            }).then(function successCallback(response){
                $location.path('/user');
            })
        };

        $scope.cancel = function(){
            $location.path('/user');
        }

    }
]);
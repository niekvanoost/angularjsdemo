blueDonutApp.controller('homeController', ['$scope', '$routeParams', '$http', '$location', '$rootScope',
    function($scope, $routeParams, $http, $location, $rootScope){
        $scope.header = "Welcome to this AngularJS Demo";
        $scope.subheader = "Simple CRUD via CodeIgniter w/ Restfull server, AngularJS and Bootstrap";
        
        $scope.creator = {
            name: "Niek van Oost",
            email:  "niekv@noost.nu",
            phone: "06-27539828"
        };

        $scope.info = {
            codeigniter: "https://www.codeigniter.com/",
            restserver: "https://github.com/chriskacerguis/codeigniter-restserver",
            bootstrap: "http://getbootstrap.com/"
        }
    }
]);
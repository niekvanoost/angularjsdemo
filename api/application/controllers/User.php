<?php
require APPPATH.'/libraries/REST_Controller.php';
class User extends REST_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('User_model');        
    }
	
	function index_get(){
		if(!$this->get('id')){
			$users = $this->User_model->get_all_users();
		}else{
			$users = $this->User_model->get_user($this->get('id'));
		}
		
		if($users){
			$this->response($users, 200);
		}else{
			$this->response(null, 404);
		}
	}
	
	public function index_put(){
		$realname 		= $this->put('realname');
		$username		= $this->put('username');
		
		if(!$realname){
			$this->response(array('error' => 'Missing Real name'), 400);
		}elseif(!$username){
			$this->response(array('error' => 'Missing Username'), 400);
		}else{
			$data = array(
				'realname' 		=> $realname,
				'username' 		=> $username,
			);
		}
		$user_id = $this->User_model->add_user($data);
		
		if($user_id > 0){
			$message = array(
				'id'			=> $user_id,
				'realname' 		=> $realname,
				'username' 		=> $username,
			);
			$this->response($message, 200);
		}
	}
	
	public function index_post(){
		$id 			= $this->post('id');
		$realname 		= $this->post('realname');
		$username		= $this->post('username');
		
		if(!$id){
			$this->response(array('error' => 'User id is required'), 400);
		}elseif(!$realname){
			$this->response(array('error' => 'Realname is required'), 400);
		}elseif(!$username){
			$this->response(array('error' => 'username is required'), 400);
		}else{
			$data = array(
				'realname' 		=> $realname,
				'username'		=> $username,
			);
		}
		$this->User_model->update_user($id, $data);
		$message = array('success' 	=> 'User '. $realname . ' updated!');
		$this->response($message, 200);		
	}
	
	public function deleteUser_post(){
		$id = $this->post('id');

		if ($id == NULL){
			$message = array('error' => 'Missing delete data: id');
			$this->response($message, 400);
		}else{
			$this->User_model->delete_user($id);
			$message = array("id" => $id, 'message' => 'DELETED');
			$this->response($message, 200);
		}
	}


}

?>
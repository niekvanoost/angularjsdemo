<?php

class User_Model extends CI_Model {

	public $table = 'users';

	public function __construct(){
		parent::__construct();
	}
	
	public function get_all_users(){
		$this->db->where('deleted', "0");
		$query = $this->db->get($this->table);
		return $query->result();
	}
	
	public function get_user($id){
		$this->db->where('id', $id);
		$query = $this->db->get($this->table);
		return $query->row_array();
	}

	public function add_user($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	public function update_user($id, $data){
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
	}
	
	public function delete_user($id){
		$data = array(
			"deleted" => 1
		);
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
	}


}